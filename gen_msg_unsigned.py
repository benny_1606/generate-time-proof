#!/usr/bin/env python3
# Requires feedparser installed
import feedparser, datetime
from pathlib import Path
from sys import exit

dir_path = Path(__file__).resolve().parent
msg_path = dir_path.joinpath("msg")
output_path = msg_path.with_suffix(".out")

output = None

if Path(msg_path).is_file():
    with open(msg_path, 'r') as f:
        msg = """++++ Message Begins Here ++++
"""
        msg += f.read()
        msg += """
++++ Message Ends Here ++++

"""
        output = msg
else:
    print(str(msg_path) + " is required as message")
    exit(1)


url = 'https://www.hongkongfp.com/feed/'

date_now = datetime.datetime.now(datetime.timezone.utc).isoformat()

header = str(date_now)
header += """

To proof that this message is created later than
the date stated (in ISO 8601 format) immediately above,
the following news headlines are included.

++++ News Headlines Begin Here ++++
"""

output += header

f = feedparser.parse(url)

news = f"""
Feed Title:
{f['feed']['title']}
Feed URL:
{url}
URL to website:
{f['feed']['link']}

"""
output += news

for i in range(0,10):
    output += f['entries'][i]['title'] + "\n"
    
footer = """
++++ News Headlines End Here ++++
"""

output += footer

with open(output_path, "w") as f:
    f.write(output)