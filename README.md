# Generate Time Proof
aka Enduring Freedom.

A tiny Python script aims to prove the time of the message.

The script has the following actions,

* [Latest news headlines from Hong Kong Free Press][HKFP] are included
* Current timestamp in [ISO 8601](https://docs.python.org/3/library/datetime.html#datetime.date.isoformat) format is added

[HKFP]: https://hongkongfp.com/

# Limitation
1. The generated message has **NEITHER** integrity nor authenticity.
	* Anyone can alter/replace the message, to falsify the actual date of creation. 
	* GPG signing is thus **strongly** recommended.
	
2. The new headlines can only provided time proof that, the message
	* cannot be created prior to the timestamp, e.g. 2019-07-21
	* however, the message can be created after the generation, e.g. 2019-07-23
	* See Figure below, or [in png](doc/limitation_timeline.png)

![Figure of illustrating the limitations of the time proof, as discussed above.](doc/limitation_timeline.svg)

# Requirement
* Python 3+ (developed from 3.8)
* feedparser
	* installed by `pip3 install feedparser`

# Usage
## 1. Download
For downloading v1.0,

Choose one of the following:

1. Simple way: Download [gen_msg_unsigned.py](https://gitlab.com/end_my_suffering/generate-time-proof/-/raw/v1.0/gen_msg_unsigned.py), by `curl -O https://gitlab.com/end_my_suffering/generate-time-proof/-/raw/v1.0/gen_msg_unsigned.py` or `wget https://gitlab.com/end_my_suffering/generate-time-proof/-/raw/v1.0/gen_msg_unsigned.py` or any other ways you preferred.

2. Or, `git clone` followed by `git checkout tags/v1.0`, assuming you know what you are doing.

## 2. Verification (Optional)
Recommended if you cares about security.

You don't want to download a random program to your devices (full of private info), and execute it without think twice, right?

### a. Downloaded using "Simple way"
If you choose the "Simple way" in the previous step, run `sha256sum gen_msg_unsigned.py`.

The result should be exactly as below,
```
ec7e63c318405f3820defa1c34e6224a5729ca0263f73a7d35533327917376eb  gen_msg_unsigned.py
```

### b. Downloaded with `git clone`
If you choose `git clone` in the previous step, `verify-tag`, `verify-commit` and `git log --show-signature` should resulted in GPG key `1447D8586AB05572F1F3304BB85F978249AD8727`.

#### Note on GPG Key
For the Chain of Trust and public key, see [this GitLab Snippet](https://gitlab.com/snippets/1984392).

### Evaluate the safety of the file
Now you can reasonably believe the script is written by me.

However, as I said, it is still a random script on the internet to you.

I recommend you to inspect the file `gen_msg_unsigned.py` line by line, if you understand Python.

If not, execute the script in a [VM (Virtual Machine)](https://en.wikipedia.org/wiki/Virtual_machine). I recommend [Whonix](https://www.whonix.org/) ([FAQ](https://www.whonix.org/wiki/FAQ)).

If not, execute it in a [sandbox](https://en.wikipedia.org/wiki/Sandbox_(computer_security)). I recommend [Firejail](https://wiki.archlinux.org/index.php/Firejail).

If not, upload it to VirusTotal [(Wikipedia)](https://en.wikipedia.org/wiki/VirusTotal), a meta-antivirus.

If not, execute it with cautions, e.g. an separated user unprivileged account. See [Archwiki on Security](https://wiki.archlinux.org/index.php/Security).

Or all the precautions above!
## 3. Run

1. Create/modify `msg` file under the same directory of the script
2. Run the script with `python gen_msg_unsigned.py`
3. `msg.out` is generated, GPG signing of which is **strongly** recommended

# Example
`msg`:
```
Stand with Hong Kong
```

`msg_out`:
```
++++ Message Begins Here ++++
Stand with Hong Kong
++++ Message Ends Here ++++

2020-06-07T16:57:00.496855+00:00

To proof that this message is created later than
the date stated (in ISO 8601 format) immediately above,
the following news headlines are included.

++++ News Headlines Begin Here ++++

Feed Title:
Hong Kong Free Press HKFP
Feed URL:
https://www.hongkongfp.com/feed/
URL to website:
https://hongkongfp.com

Lessons from history: Intervention in Hong Kong was predicted, resistance is not futile
Beijing’s national security law dilemma: Transparency… with Hongkongers?
‘Police will come for my son’: Hong Kong parents seek a way out for their children
The Hong Kong movement must stand with Black Lives Matter
Hong Kong protesters seek sanctuary overseas as noose tightens
US accuses China of using Floyd death for ‘laughable propaganda’
Taiwan populist mayor Han Kuo-yu recalled in historic vote
Anti-virus face masks plague Hong Kong’s beaches
Taiwan should improve its laws to help asylum-seeking Hongkongers
In Carrie Lam’s letter, Hong Kong’s rule of law is sacrificed to state paranoia

++++ News Headlines End Here ++++

```

# License
```
MIT License

Copyright (c) 2020 Adam S, Stand with Hong Kong
```

# Acknowledgement
The following software(s) is used,

* [feedparser](https://github.com/kurtmckee/feedparser)
	* `Copyright (C) 2010-2020 Kurt McKee <contactme@kurtmckee.org>`
	* `Copyright (C) 2002-2008 Mark Pilgrim`
	* [Custom, OSI Approved License](https://github.com/kurtmckee/feedparser/blob/develop/LICENSE)